# Esplora Snake clone

A clone of the iconic Snake game for Arduino Esplora

# Rules

* Eat food to increase snake's length and your score
* Each subsequent piece of food is worth more and more points
* If snake runs into a wall or itself, game is over
* After eating at least 20 pieces of food, the walls turn green and you can choose to exit
* If you exit you get more points for every segment of snake that survived
* If snake dies you don't get any extra points

# Gameplay

Eating food

![Eating food](https://gitlab.com/bohundan/esplora-snake-clone/-/raw/master/Illustrations/eating_food.gif)

Snake runs into wall

![Snake runs into wall](https://gitlab.com/bohundan/esplora-snake-clone/-/raw/master/Illustrations/snake_runs_into_border.gif)

Snake runs into itself

![Snake runs into itself](https://gitlab.com/bohundan/esplora-snake-clone/-/raw/master/Illustrations/snake_runs_into_itself.gif)

Snake eats 20th piece of food and exits

![Snake eats 20th piece of food and exits](https://gitlab.com/bohundan/esplora-snake-clone/-/raw/master/Illustrations/slithering_away.gif)

# Controls

You can use the analog joystick or 4 buttons to control the snake. (Snake can't do a 180° turn and only your latest input is counted, ie. if your press left and then right between 2 ticks, snake will go right.)

For menu navigation, use top/bottom button to go up/down and press joystick to confirm (enter).

![Arduino Esplora](https://farm9.staticflickr.com/8069/8209014766_1b5a58e3c2_c.jpg)
