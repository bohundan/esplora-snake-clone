#ifndef SNAKE_H
#define SNAKE_H

#include <Esplora.h>
#include <SPI.h>
#include <TFT.h>
#include "LinkedList.h" // From https://github.com/ivanseidel/LinkedList/archive/master.zip
#include "Segment.h"
#include "Constants.h"
#include "Sound.h"

class Snake {
private:
  // Snake represented as a linked list
  LinkedList<Segment> body;
  int growing;
public:
  // Initialize RNG
  static void InitRandom ();
  /* Constructor takes initial length (3 by default) and draws a new snake on display, facing horizontally towards the center
   * Sets direction (LEFT or RIGHT in this case)
   */
  Snake ( int & direction, int length = 3 );
  // Check if any part of snake sits on segment (x and y coords)
  bool Occupies ( Segment segment );
  /* Move snake in a direction
   * Returns false if snake collided with something
   */
  bool Move ( int direction );
  // Snake's length will increase over _amount_ of moves
  void Grow ( int amount );
  // Snake slithers out of view
  void SlitherAway ();
  // Access to first body segment
  Segment Head ();
  // Length of snake (how many segments is snake made out of)
  int Length ();
  // Cause of death
  bool CollidedWithBorder ();
};

void Snake::InitRandom () {
  unsigned long seed = millis () + Esplora.readLightSensor () * Esplora.readMicrophone () * Esplora.readTemperature ( DEGREES_F );
  randomSeed ( seed );
}

Snake::Snake ( int & direction, int length ) {
  body = LinkedList<Segment> ();
  growing = 0;

  // Horizontal position clamped by length
  int x = ( random ( length, 38 - length ) * 4 ) + 2,
  // Verical position any except top/bottom row
  y = ( random ( 1, 29 ) * 4 ) + 2,
  // Coefficient (4 = size of segment) positive = grow tail towards right side, negative = towards left side
  n = 4;
  // Add head
  body.add ( { x, y } );
  if ( x < 82 ) {
    direction = constants::RIGHT;
    n *= -1;
  }
  else
    direction = constants::LEFT;
  // Add body facing away from center horizontally
  for ( int i = 1; i < length; i++ )
    body.add ( { x + ( n * i ), y } );

  // Draw first snake
  EsploraTFT.fill ( 255, 0, 0 );
  Segment s;
  for ( int i = 0; i < body.size (); i++ ) {
    s = body.get ( i );
    EsploraTFT.rect ( s.x, s.y, 4, 4 );
  }
}

bool Snake::Occupies ( Segment segment ) {
  Segment s;
  // Check if all body segments
  for ( int i = 0; i < body.size (); i++ ) {
    s = body.get ( i );
    if ( s.x == segment.x && s.y == segment.y )
      return true;
  }
  return false;
}

bool Snake::Move ( int direction ) {
  // Move snake (add new head)
  Segment head = body.get ( 0 );
  switch ( direction )
  {
    case constants::RIGHT: body.unshift ( { head.x + 4, head.y } ); break;
    case constants::LEFT:  body.unshift ( { head.x - 4, head.y } ); break;
    case constants::UP:    body.unshift ( { head.x, head.y - 4 } ); break;
    case constants::DOWN:  body.unshift ( { head.x, head.y + 4 } );
  }
  
  // Mask (dead) tail if not growing
  if ( growing == 0 ) {
    Segment tail = body.pop ();
    EsploraTFT.fill ( 0, 0, 0 );
    EsploraTFT.rect ( tail.x, tail.y, 4, 4 );
  }
  else
    growing--;

  // Draw new head
  EsploraTFT.fill ( 255, 0, 0 );
  head = body.get ( 0 );
  // Don't try to draw outside of borders
  int x = head.x;
  int y = head.y;
  if ( x < 2 ) x = 2;
  else if ( x > 154 ) x = 154;
  if ( y < 2 ) y = 2;
  else if ( y > 122 ) y = 122;
  // Draw head
  EsploraTFT.rect ( x, y, 4, 4 );

  // Check if snake collided with border
  if ( head.x < 2 || head.x > 154 || head.y < 2 || head.y > 122 )
    return false;

  // Check if snake collided with itself
  Segment s;
  for ( int i = 1; i < body.size (); i++ ) {
    s = body.get ( i );
    if ( s.x == head.x && s.y == head.y )
      return false;
  }

  return true;
}

void Snake::Grow ( int amount ) {
  growing += amount;
}

void Snake::SlitherAway () {
  while ( body.size () != 1 ) {
    Segment tail = body.pop ();
    EsploraTFT.fill ( 0, 0, 0 );
    EsploraTFT.rect ( tail.x, tail.y, 4, 4 );
    Sound::FoodEaten ();
    delay ( 60 );
  }
}

Segment Snake::Head () {
  return body.get ( 0 );
}

int Snake::Length () {
  return body.size ();
}

bool Snake::CollidedWithBorder () {
  Segment head = body.get ( 0 );
  return ( head.x < 2 || head.x > 154 || head.y < 2 || head.y > 122 );
}

#endif
