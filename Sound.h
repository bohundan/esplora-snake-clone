#ifndef SOUND_H
#define SOUND_H

#include <Esplora.h>

class Sound {
public:
  // 750 ms long
  static void StartTune ();
  // Doesn't use delay
  static void FoodEaten ();
  // 1250 ms long
  static void SnakeSurvived ();
  // 1750 ms long
  static void SnakeDied ();
};

void Sound::StartTune () {
  Esplora.tone ( 698, 125 ); // F5
  delay ( 125 );
  Esplora.tone ( 493, 125 ); // B4
  delay ( 125 );
  Esplora.tone ( 466, 125 ); // A#4
  delay ( 125 );
  Esplora.tone ( 622, 125 ); // D#5
  delay ( 125 );
  Esplora.tone ( 783, 125 ); // G5
  delay ( 125 );
  Esplora.tone ( 830, 125 ); // G#5
  delay ( 125 );
  Esplora.tone ( 880, 375 ); // A5
}

void Sound::FoodEaten () {
  Esplora.tone ( 3000, 50 );
}

void Sound::SnakeSurvived () {
  Esplora.tone ( 1397, 125 ); // F6
  delay ( 125 );
  Esplora.tone ( 988, 125 ); // B5
  delay ( 125 );
  Esplora.tone ( 831, 125 ); // G#5
  delay ( 125 );
  Esplora.tone ( 1245, 125 ); // D#6
  delay ( 125 );
  Esplora.tone ( 988, 125 ); // B5
  delay ( 125 );
  Esplora.tone ( 659, 125 ); // E5
  delay ( 125 );
  Esplora.tone ( 831, 125 ); // G#5
  delay ( 125 );
  Esplora.tone ( 1319, 125 ); // E6
  delay ( 125 );
  Esplora.tone ( 1661, 250 ); // G#6
  delay ( 250 );
}

void Sound::SnakeDied () {
  Esplora.tone ( 1046, 125 ); // C6
  delay ( 125 );
  Esplora.tone ( 880, 125 );  // A5
  delay ( 125 );
  Esplora.tone ( 740, 125 );  // F#5
  delay ( 125 );
  Esplora.tone ( 622, 125 );  // D#5
  delay ( 125 );
  Esplora.tone ( 523, 125 );  // C5
  delay ( 125 );
  Esplora.tone ( 440, 125 );  // A4
  delay ( 125 );
  Esplora.tone ( 392, 125 );  // G4
  delay ( 125 );
  Esplora.tone ( 370, 125 );  // F#4
  delay ( 125 );
  Esplora.tone ( 349, 750 );  // F4
  delay ( 750 );
}

#endif
