#ifndef DISPLAY_H
#define DISPLAY_H

#include <Esplora.h>
#include <SPI.h>
#include <TFT.h>

class Display {
private:
  // Draws snake bitart inspired by https://www.brik.co/blogs/designs/snake
  static void DrawSnake ();
  // Draws snake's skeleton
  static void DrawSkeleton ();
public:
  // Draws 2 pixel wide borders around the screen of given RGB color
  static void DrawBorders ( int r, int g, int b );
  // Displays welcome message and bitart
  static void SplashScreen ();
  // Displays score and bitart
  static void EndScreen ( bool survived, unsigned long score );
  // Draws arrow in front of "Replay"
  static void ReplaySelected ();
  // Draws arrow in front of "Return"
  static void ReturnSelected ();
};

void Display::DrawBorders ( int r, int g, int b ) {
  EsploraTFT.noStroke ();
  EsploraTFT.fill ( r, g, b );
  EsploraTFT.rect ( 0, 0, 160, 2 );
  EsploraTFT.rect ( 0, 0, 2, 128 );
  EsploraTFT.rect ( 0, 126, 160, 2 );
  EsploraTFT.rect ( 158, 0, 2, 128 );
}

void Display::SplashScreen () {
  EsploraTFT.background ( 0, 0, 0 );
  DrawSnake ();
  EsploraTFT.stroke ( 255, 255 , 255 );
  EsploraTFT.setTextSize ( 2 );
  EsploraTFT.text ( "Welcome to:", 12, 12 );
  EsploraTFT.setTextSize ( 4 );
  EsploraTFT.text ( "SNAKE", 12, 35 );
  EsploraTFT.setTextSize ( 1 );
  EsploraTFT.text ( "Press joystick\nto start.", 12, 94 );
}

void Display::EndScreen ( bool survived, unsigned long score ) {
  EsploraTFT.background ( 0, 0, 0 );
  EsploraTFT.setTextSize ( 2 );
  if ( survived ) {
    EsploraTFT.stroke ( 0, 255 , 0 );
    EsploraTFT.text ( "Snake survived", 12, 12 );
    DrawSnake ();
  }
  else {
    EsploraTFT.stroke ( 255, 0 , 0 );
    EsploraTFT.text ( "Snake died", 12, 12 );
    DrawSkeleton ();
  }
  EsploraTFT.stroke ( 255, 255 , 255 );
  EsploraTFT.text ( "Score:", 12, 34 );
  // Conversion of int to char array
  EsploraTFT.text ( String ( score ).c_str (), 12, 56 );

  EsploraTFT.setTextSize ( 1 );
  EsploraTFT.text ( "Replay\nReturn", 12, 94 );
}

void Display::ReplaySelected () {
  // Mask with black rectangle
  EsploraTFT.noStroke ();
  EsploraTFT.fill ( 0, 0 , 0 );
  // https://www.arduino.cc/en/Reference/TFTSetTextSize says size 1 character is 10 pixels tall, size 2 20 pixels tall
  // https://www.arduino.cc/en/Guide/TFT says default size is 5x8 pixels, size 2 is 10x16, size 3 15x24 and so on
  // Assuming that it's 5x8, draw black rectangle over both arrow's positions to clear
  EsploraTFT.rect ( 7, 94, 5, 16 );
  // Draw arrow on first row (in front of "Replay")
  EsploraTFT.stroke ( 255, 255 , 255 );
  EsploraTFT.setTextSize ( 1 );
  EsploraTFT.text ( ">", 7, 94 );
}

void Display::ReturnSelected () {
  // Mask with black rectangle
  EsploraTFT.noStroke ();
  EsploraTFT.fill ( 0, 0 , 0 );
  EsploraTFT.rect ( 7, 94, 5, 16 );
  EsploraTFT.stroke ( 255, 255 , 255 );
  // Draw arrow on second row (in front of "Return")
  EsploraTFT.setTextSize ( 1 );
  EsploraTFT.text ( "\n>", 7, 94 );
}

void Display::DrawSnake () {
  /* Bitart out of rectangles
   * This way it's impossible to edit/change 
   * A better way would be to upload the image to an SD card and load it, but what is done is done
   */
  EsploraTFT.noStroke ();
  EsploraTFT.fill ( 255, 114, 0 );
  EsploraTFT.rect ( 121, 94, 28, 8 );
  EsploraTFT.rect ( 105, 69, 40, 8 );
  EsploraTFT.fill ( 255, 0, 0 );
  EsploraTFT.rect ( 101, 77, 12, 12 );
  EsploraTFT.rect ( 97, 89, 36, 8 );
  EsploraTFT.rect ( 133, 93, 16, 12 );
  EsploraTFT.fill ( 255, 114, 0 );
  EsploraTFT.rect ( 101, 77, 4, 4 );
  EsploraTFT.rect ( 97, 81, 4, 8 );
  EsploraTFT.rect ( 101, 89, 12, 4 );
  EsploraTFT.rect ( 113, 85, 20, 4 );
  EsploraTFT.rect ( 133, 89, 4, 4 );
  EsploraTFT.rect ( 137, 93, 4, 4 );
  EsploraTFT.rect ( 137, 101, 4, 4 );
  EsploraTFT.rect ( 137, 93, 4, 4 );
  EsploraTFT.rect ( 137, 105, 8, 12 );
  EsploraTFT.rect ( 125, 109, 12, 12 );
  EsploraTFT.rect ( 117, 101, 8, 8 );
  EsploraTFT.rect ( 105, 109, 12, 12 );
  EsploraTFT.fill ( 255, 0, 0 );
  EsploraTFT.rect ( 121, 53, 4, 4 );
  EsploraTFT.rect ( 125, 57, 8, 4 );
  EsploraTFT.rect ( 133, 53, 16, 4 );
  EsploraTFT.rect ( 149, 57, 8, 4 );
  EsploraTFT.rect ( 153, 61, 4, 12 );
  EsploraTFT.rect ( 149, 69, 4, 8 );
  EsploraTFT.rect ( 145, 73, 4, 8 );
  EsploraTFT.rect ( 129, 77, 16, 4 );
  EsploraTFT.rect ( 109, 73, 24, 4 );
  EsploraTFT.rect ( 145, 105, 4, 12 );
  EsploraTFT.rect ( 137, 117, 8, 4 );
  EsploraTFT.rect ( 129, 121, 8, 4 );
  EsploraTFT.rect ( 125, 117, 4, 4 );
  EsploraTFT.rect ( 121, 109, 4, 8 );
  EsploraTFT.rect ( 117, 105, 4, 8 );
  EsploraTFT.rect ( 113, 101, 4, 8 );
  EsploraTFT.rect ( 97, 109, 4, 8 );
  EsploraTFT.rect ( 105, 105, 4, 8 );
  EsploraTFT.rect ( 109, 109, 4, 4 );
  EsploraTFT.rect ( 109, 101, 4, 4 );
  EsploraTFT.rect ( 101, 101, 4, 16 );
  EsploraTFT.fill ( 128, 128, 128 );
  EsploraTFT.rect ( 97, 105, 4, 4 );
  EsploraTFT.rect ( 105, 113, 4, 4 );
  EsploraTFT.fill ( 128, 128, 128 );
  EsploraTFT.rect ( 125, 49, 8, 4 );
  EsploraTFT.rect ( 145, 61, 4, 8 );
  EsploraTFT.rect ( 133, 69, 12, 4 );
  EsploraTFT.rect ( 105, 69, 4, 4 );
  EsploraTFT.rect ( 97, 93, 4, 4 );
  EsploraTFT.rect ( 117, 93, 4, 4 );
  EsploraTFT.rect ( 141, 93, 8, 4 );
  EsploraTFT.rect ( 145, 97, 4, 4 );
  EsploraTFT.rect ( 133, 101, 4, 4 );
  EsploraTFT.rect ( 137, 105, 4, 8 );
  EsploraTFT.rect ( 121, 109, 8, 4 );
  EsploraTFT.rect ( 133, 113, 4, 4 );
  EsploraTFT.rect ( 121, 101, 4, 4 );
}

void Display::DrawSkeleton () {
  // Bitart out of rectangles
  EsploraTFT.noStroke ();
  EsploraTFT.fill ( 60, 60, 60 );
  EsploraTFT.rect ( 121, 54, 2, 2 );
  EsploraTFT.rect ( 125, 55, 1, 1 );
  EsploraTFT.rect ( 124, 57, 1, 1 );
  EsploraTFT.rect ( 127, 55, 1, 1 );
  EsploraTFT.rect ( 128, 56, 1, 1 );
  EsploraTFT.rect ( 127, 58, 1, 1 );
  EsploraTFT.rect ( 131, 57, 1, 1 );
  EsploraTFT.rect ( 131, 55, 1, 1 );
  EsploraTFT.rect ( 133, 54, 1, 1 );
  EsploraTFT.rect ( 136, 57, 1, 1 );
  EsploraTFT.rect ( 136, 54, 1, 1 );
  EsploraTFT.rect ( 137, 53, 1, 1 );
  EsploraTFT.rect ( 141, 56, 1, 1 );
  EsploraTFT.rect ( 142, 53, 1, 1 );
  EsploraTFT.rect ( 142, 55, 2, 1 );
  EsploraTFT.rect ( 147, 56, 1, 1 );
  EsploraTFT.rect ( 148, 53, 1, 1 );
  EsploraTFT.rect ( 152, 56, 1, 1 );
  EsploraTFT.rect ( 153, 58, 1, 1 );
  EsploraTFT.rect ( 150, 60, 2, 1 );
  EsploraTFT.rect ( 153, 60, 1, 1 );
  EsploraTFT.rect ( 154, 62, 1, 1 );
  EsploraTFT.rect ( 151, 65, 1, 1 );
  EsploraTFT.rect ( 154, 64, 1, 1 );
  EsploraTFT.rect ( 152, 70, 1, 1 );
  EsploraTFT.rect ( 149, 70, 1, 1 );
  EsploraTFT.rect ( 146, 73, 1, 1 );
  EsploraTFT.rect ( 144, 74, 1, 1 );
  EsploraTFT.rect ( 147, 76, 1, 1 );
  EsploraTFT.rect ( 135, 74, 1, 1 );
  EsploraTFT.rect ( 136, 78, 1, 1 );
  EsploraTFT.rect ( 130, 76, 1, 1 );
  EsploraTFT.rect ( 125, 71, 1, 1 );
  EsploraTFT.rect ( 120, 76, 1, 1 );
  EsploraTFT.rect ( 115, 71, 2, 1 );
  EsploraTFT.rect ( 108, 74, 2, 1 );
  EsploraTFT.rect ( 111, 77, 2, 1 );
  EsploraTFT.rect ( 106, 79, 3, 1 );
  EsploraTFT.rect ( 101, 81, 4, 5 );
  EsploraTFT.rect ( 100, 91, 8, 2 );
  EsploraTFT.rect ( 106, 95, 1, 1 );
  EsploraTFT.rect ( 112, 90, 2, 5 );
  EsploraTFT.rect ( 118, 87, 8, 4 );
  EsploraTFT.rect ( 130, 90, 4, 3 );
  EsploraTFT.rect ( 135, 94, 3, 5 );
  EsploraTFT.rect ( 140, 100, 5, 3 );
  EsploraTFT.rect ( 143, 108, 5, 1 );
  EsploraTFT.rect ( 138, 116, 4, 4 );
  EsploraTFT.rect ( 130, 115, 2, 6 );
  EsploraTFT.rect ( 122, 109, 3, 6 );
  EsploraTFT.rect ( 121, 115, 1, 1 );
  EsploraTFT.rect ( 115, 108, 1, 1 );
  EsploraTFT.rect ( 113, 103, 1, 1 );
  EsploraTFT.rect ( 110, 106, 2, 3 );
  EsploraTFT.rect ( 104, 103, 4, 4 );
  EsploraTFT.rect ( 99, 105, 1, 3 );
  EsploraTFT.rect ( 100, 104, 1, 1 );
  EsploraTFT.rect ( 103, 108, 1, 1 );
  EsploraTFT.rect ( 101, 109, 1, 1 );
  EsploraTFT.rect ( 100, 111, 1, 1 );
  EsploraTFT.rect ( 102, 113, 1, 1 );
  EsploraTFT.rect ( 103, 114, 1, 1 );
  EsploraTFT.rect ( 104, 112, 1, 1 );
  EsploraTFT.rect ( 105, 113, 1, 1 );
  EsploraTFT.rect ( 106, 111, 1, 1 );
  EsploraTFT.rect ( 107, 112, 1, 1 );
  EsploraTFT.rect ( 108, 111, 1, 1 );
  EsploraTFT.fill ( 192, 192, 192 );
  EsploraTFT.rect ( 122, 55, 3, 1 );
  EsploraTFT.rect ( 124, 54, 4, 1 );
  EsploraTFT.rect ( 127, 53, 2, 1 );
  EsploraTFT.rect ( 123, 56, 5, 1 );
  EsploraTFT.rect ( 126, 55, 1, 5 );
  EsploraTFT.rect ( 123, 57, 1, 2 );
  EsploraTFT.rect ( 127, 57, 4, 1 );
  EsploraTFT.rect ( 129, 56, 9, 1 );
  EsploraTFT.rect ( 130, 54, 1, 6 );
  EsploraTFT.rect ( 131, 59, 1, 2 );
  EsploraTFT.rect ( 131, 54, 1, 1 );
  EsploraTFT.rect ( 132, 53, 1, 3 );
  EsploraTFT.rect ( 133, 55, 9, 1 );
  EsploraTFT.rect ( 135, 53, 1, 7 );
  EsploraTFT.rect ( 136, 52, 1, 2 );
  EsploraTFT.rect ( 137, 52, 1, 1 );
  EsploraTFT.rect ( 136, 59, 1, 2 );
  EsploraTFT.rect ( 137, 54, 12, 1 );
  EsploraTFT.rect ( 142, 51, 2, 1 );
  EsploraTFT.rect ( 141, 52, 2, 1 );
  EsploraTFT.rect ( 140, 53, 2, 1 );
  EsploraTFT.rect ( 140, 56, 1, 3 );
  EsploraTFT.rect ( 141, 58, 1, 2 );
  EsploraTFT.rect ( 144, 55, 7, 1 );
  EsploraTFT.rect ( 146, 56, 1, 3 );
  EsploraTFT.rect ( 147, 52, 1, 2 );
  EsploraTFT.rect ( 148, 52, 1, 1 );
  EsploraTFT.rect ( 148, 51, 3, 1 );
  EsploraTFT.rect ( 148, 56, 4, 1 );
  EsploraTFT.rect ( 150, 57, 7, 1 );
  EsploraTFT.rect ( 153, 56, 1, 1 );
  EsploraTFT.rect ( 150, 58, 3, 2 );
  EsploraTFT.rect ( 149, 59, 1, 3 );
  EsploraTFT.rect ( 148, 61, 1, 1 );
  EsploraTFT.rect ( 152, 60, 1, 10 );
  EsploraTFT.rect ( 153, 61, 1, 7 );
  EsploraTFT.rect ( 150, 64, 2, 1 );
  EsploraTFT.rect ( 149, 65, 2, 1 );
  EsploraTFT.rect ( 154, 63, 2, 1 );
  EsploraTFT.rect ( 155, 64, 2, 1 );
  EsploraTFT.rect ( 156, 65, 1, 1 );
  EsploraTFT.rect ( 151, 68, 1, 5 );
  EsploraTFT.rect ( 147, 69, 7, 1 );
  EsploraTFT.rect ( 152, 72, 1, 5 );
  EsploraTFT.rect ( 145, 74, 3, 2 );
  EsploraTFT.rect ( 143, 75, 4, 2 );
  EsploraTFT.rect ( 146, 77, 2, 1 );
  EsploraTFT.rect ( 146, 81, 2, 1 );
  EsploraTFT.rect ( 147, 78, 1, 3 );
  EsploraTFT.rect ( 141, 71, 2, 1 );
  EsploraTFT.rect ( 142, 72, 3, 1 );
  EsploraTFT.rect ( 144, 73, 2, 1 );
  EsploraTFT.rect ( 147, 73, 2, 2 );
  EsploraTFT.rect ( 148, 72, 2, 2 );
  EsploraTFT.rect ( 149, 71, 1, 1 );
  EsploraTFT.rect ( 150, 70, 1, 3 );
  EsploraTFT.rect ( 134, 76, 10, 2 );
  EsploraTFT.rect ( 131, 75, 9, 2 );
  EsploraTFT.rect ( 128, 73, 4, 3 );
  EsploraTFT.rect ( 115, 72, 15, 3 );
  EsploraTFT.rect ( 108, 75, 3, 3 );
  EsploraTFT.rect ( 110, 74, 4, 3 );
  EsploraTFT.rect ( 112, 73, 4, 3 );
  EsploraTFT.rect ( 135, 71, 1, 2 );
  EsploraTFT.rect ( 136, 72, 1, 3 );
  EsploraTFT.rect ( 137, 74, 1, 1 );
  EsploraTFT.rect ( 137, 78, 1, 2 );
  EsploraTFT.rect ( 136, 79, 1, 3 );
  EsploraTFT.rect ( 135, 81, 1, 1 );
  EsploraTFT.rect ( 132, 74, 3, 1 );
  EsploraTFT.rect ( 129, 76, 1, 3 );
  EsploraTFT.rect ( 128, 78, 1, 2 );
  EsploraTFT.rect ( 124, 69, 2, 1 );
  EsploraTFT.rect ( 125, 70, 2, 1 );
  EsploraTFT.rect ( 126, 71, 2, 1 );
  EsploraTFT.rect ( 120, 75, 2, 1 );
  EsploraTFT.rect ( 120, 77, 2, 1 );
  EsploraTFT.rect ( 121, 76, 1, 1 );
  EsploraTFT.rect ( 115, 70, 3, 1 );
  EsploraTFT.rect ( 117, 71, 3, 1 );
  EsploraTFT.rect ( 113, 77, 1, 2 );
  EsploraTFT.rect ( 114, 78, 1, 2 );
  EsploraTFT.rect ( 106, 73, 5, 1 );
  EsploraTFT.rect ( 106, 74, 1, 1 );
  EsploraTFT.rect ( 107, 76, 1, 2 );
  EsploraTFT.rect ( 106, 77, 1, 1 );
  EsploraTFT.rect ( 105, 78, 3, 3 );
  EsploraTFT.rect ( 108, 78, 1, 1 );
  EsploraTFT.rect ( 104, 79, 2, 3 );
  EsploraTFT.rect ( 108, 80, 1, 2 );
  EsploraTFT.rect ( 109, 81, 1, 2 );
  EsploraTFT.rect ( 110, 82, 1, 2 );
  EsploraTFT.rect ( 102, 80, 2, 7 );
  EsploraTFT.rect ( 100, 80, 2, 1 );
  EsploraTFT.rect ( 98, 81, 3, 1 );
  EsploraTFT.rect ( 104, 82, 1, 2 );
  EsploraTFT.rect ( 101, 83, 2, 10 );
  EsploraTFT.rect ( 100, 85, 1, 7 );
  EsploraTFT.rect ( 99, 90, 1, 4 );
  EsploraTFT.rect ( 98, 93, 1, 2 );
  EsploraTFT.rect ( 104, 86, 4, 1 );
  EsploraTFT.rect ( 105, 85, 2, 1 );
  EsploraTFT.rect ( 102, 91, 3, 3 );
  EsploraTFT.rect ( 105, 89, 1, 8 );
  EsploraTFT.rect ( 104, 94, 5, 1 );
  EsploraTFT.rect ( 108, 87, 1, 2 );
  EsploraTFT.rect ( 106, 88, 1, 2 );
  EsploraTFT.rect ( 107, 88, 1, 1 );
  EsploraTFT.rect ( 106, 92, 8, 2 );
  EsploraTFT.rect ( 106, 96, 1, 3 );
  EsploraTFT.rect ( 107, 98, 1, 1 );
  EsploraTFT.rect ( 108, 91, 5, 1 );
  EsploraTFT.rect ( 113, 90, 4, 3 );
  EsploraTFT.rect ( 112, 94, 1, 2 );
  EsploraTFT.rect ( 113, 95, 1, 2 );
  EsploraTFT.rect ( 114, 96, 2, 1 );
  EsploraTFT.rect ( 111, 89, 1, 2 );
  EsploraTFT.rect ( 114, 85, 2, 1 );
  EsploraTFT.rect ( 113, 86, 2, 1 );
  EsploraTFT.rect ( 112, 87, 2, 1 );
  EsploraTFT.rect ( 111, 88, 2, 1 );
  EsploraTFT.rect ( 117, 86, 1, 8 );
  EsploraTFT.rect ( 116, 88, 1, 2 );
  EsploraTFT.rect ( 115, 89, 1, 1 );
  EsploraTFT.rect ( 118, 93, 1, 3 );
  EsploraTFT.rect ( 119, 95, 1, 1 );
  EsploraTFT.rect ( 118, 86, 1, 1 );
  EsploraTFT.rect ( 118, 85, 2, 1 );
  EsploraTFT.rect ( 119, 84, 3, 1 );
  EsploraTFT.rect ( 118, 88, 3, 3 );
  EsploraTFT.rect ( 123, 87, 2, 4 );
  EsploraTFT.rect ( 122, 87, 1, 7 );
  EsploraTFT.rect ( 120, 87, 2, 3 );
  EsploraTFT.rect ( 123, 93, 1, 3 );
  EsploraTFT.rect ( 124, 95, 1, 1 );
  EsploraTFT.rect ( 124, 86, 2, 1 );
  EsploraTFT.rect ( 125, 85, 6, 1 );
  EsploraTFT.rect ( 125, 88, 4, 3 );
  EsploraTFT.rect ( 128, 89, 5, 3 );
  EsploraTFT.rect ( 129, 92, 1, 6 );
  EsploraTFT.rect ( 133, 89, 5, 1 );
  EsploraTFT.rect ( 137, 90, 2, 1 );
  EsploraTFT.rect ( 134, 88, 1, 1 );
  EsploraTFT.rect ( 133, 91, 1, 5 );
  EsploraTFT.rect ( 131, 92, 4, 1 );
  EsploraTFT.rect ( 132, 93, 9, 1 );
  EsploraTFT.rect ( 134, 94, 1, 8 );
  EsploraTFT.rect ( 135, 101, 1, 1 );
  EsploraTFT.rect ( 140, 94, 2, 1 );
  EsploraTFT.rect ( 141, 95, 2, 1 );
  EsploraTFT.rect ( 135, 94, 2, 2 );
  EsploraTFT.rect ( 135, 96, 4, 1 );
  EsploraTFT.rect ( 135, 97, 5, 1 );
  EsploraTFT.rect ( 136, 98, 5, 1 );
  EsploraTFT.rect ( 138, 99, 7, 1 );
  EsploraTFT.rect ( 144, 100, 5, 1 );
  EsploraTFT.rect ( 148, 102, 2, 1 );
  EsploraTFT.rect ( 148, 101, 1, 1 );
  EsploraTFT.rect ( 144, 104, 2, 11 );
  EsploraTFT.rect ( 143, 101, 1, 7 );
  EsploraTFT.rect ( 144, 102, 1, 2 );
  EsploraTFT.rect ( 144, 100, 3, 2 );
  EsploraTFT.rect ( 141, 102, 1, 1 );
  EsploraTFT.rect ( 142, 102, 1, 3 );
  EsploraTFT.rect ( 139, 100, 1, 5 );
  EsploraTFT.rect ( 138, 104, 1, 2 );
  EsploraTFT.rect ( 142, 107, 1, 2 );
  EsploraTFT.rect ( 141, 108, 3, 1 );
  EsploraTFT.rect ( 140, 110, 1, 1 );
  EsploraTFT.rect ( 146, 107, 1, 5 );
  EsploraTFT.rect ( 147, 107, 2, 1 );
  EsploraTFT.rect ( 148, 108, 2, 1 );
  EsploraTFT.rect ( 149, 109, 2, 1 );
  EsploraTFT.rect ( 150, 110, 2, 1 );
  EsploraTFT.rect ( 151, 111, 1, 2 );
  EsploraTFT.rect ( 138, 115, 7, 1 );
  EsploraTFT.rect ( 136, 114, 3, 1 );
  EsploraTFT.rect ( 142, 114, 1, 10 );
  EsploraTFT.rect ( 141, 123, 1, 1 );
  EsploraTFT.rect ( 143, 111, 1, 6 );
  EsploraTFT.rect ( 129, 117, 11, 13 );
  EsploraTFT.rect ( 139, 116, 3, 3 );
  EsploraTFT.rect ( 130, 112, 1, 3 );
  EsploraTFT.rect ( 131, 114, 1, 2 );
  EsploraTFT.rect ( 132, 115, 1, 2 );
  EsploraTFT.rect ( 131, 120, 4, 1 );
  EsploraTFT.rect ( 129, 123, 2, 1 );
  EsploraTFT.rect ( 130, 121, 2, 1 );
  EsploraTFT.rect ( 130, 122, 1, 1 );
  EsploraTFT.rect ( 119, 117, 3, 1 );
  EsploraTFT.rect ( 121, 116, 1, 1 );
  EsploraTFT.rect ( 122, 115, 1, 1 );
  EsploraTFT.rect ( 123, 110, 1, 6 );
  EsploraTFT.rect ( 124, 11, 1, 4 );
  EsploraTFT.rect ( 122, 109, 1, 4 );
  EsploraTFT.rect ( 125, 106, 1, 10 );
  EsploraTFT.rect ( 126, 113, 1, 4 );
  EsploraTFT.rect ( 127, 114, 1, 4 );
  EsploraTFT.rect ( 128, 115, 1, 4 );
  EsploraTFT.rect ( 129, 116, 1, 1 );
  EsploraTFT.rect ( 124, 104, 1, 3 );
  EsploraTFT.rect ( 123, 104, 1, 1 );
  EsploraTFT.rect ( 112, 101, 2, 1 );
  EsploraTFT.rect ( 113, 102, 2, 1 );
  EsploraTFT.rect ( 114, 103, 2, 1 );
  EsploraTFT.rect ( 101, 104, 16, 1 );
  EsploraTFT.rect ( 108, 105, 11, 1 );
  EsploraTFT.rect ( 107, 106, 4, 1 );
  EsploraTFT.rect ( 109, 107, 1, 1 );
  EsploraTFT.rect ( 112, 106, 8, 2 );
  EsploraTFT.rect ( 119, 107, 3, 4 );
  EsploraTFT.rect ( 116, 108, 3, 2 );
  EsploraTFT.rect ( 121, 111, 1, 1 );
  EsploraTFT.rect ( 115, 109, 1, 3 );
  EsploraTFT.rect ( 114, 112, 1, 2 );
  EsploraTFT.rect ( 113, 113, 1, 1 );
  EsploraTFT.rect ( 105, 103, 2, 1 );
  EsploraTFT.rect ( 100, 105, 5, 1 );
  EsploraTFT.rect ( 99, 106, 6, 1 );
  EsploraTFT.rect ( 100, 107, 7, 1 );
  EsploraTFT.rect ( 104, 108, 1, 1 );
  EsploraTFT.rect ( 99, 108, 4, 1 );
  EsploraTFT.rect ( 102, 109, 1, 1 );
  EsploraTFT.rect ( 100, 109, 1, 2 );
  EsploraTFT.rect ( 101, 111, 1, 1 );
  EsploraTFT.rect ( 102, 114, 1, 1 );
  EsploraTFT.rect ( 104, 113, 1, 1 );
  EsploraTFT.rect ( 106, 112, 1, 1 );
  EsploraTFT.rect ( 103, 115, 2, 1 );
  EsploraTFT.rect ( 104, 114, 3, 1 );
  EsploraTFT.rect ( 106, 113, 3, 1 );
  EsploraTFT.rect ( 108, 112, 3, 1 );
  EsploraTFT.rect ( 111, 108, 2, 3 );
  EsploraTFT.rect ( 110, 109, 1, 2 );
  EsploraTFT.rect ( 109, 111, 3, 1 );
}

#endif
