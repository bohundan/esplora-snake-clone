#include <Esplora.h>
#include <SPI.h>
#include <TFT.h>
#include "Display.h"
#include "Sound.h"
#include "Constants.h"
#include "Segment.h"
#include "Snake.h"

enum GAME_STATES {
  START, SPLASH_SCREEN, RUN_GAME, MAIN_MENU_SEL, REPLAY_SEL
};
enum GAME_STATES STATE, NEXT_STATE;

unsigned long SCORE;
bool SNAKE_ALIVE;
long FOOD_VALUE;

void IncreaseFoodValue () {
  FOOD_VALUE += 10;
}

bool ReadDirection ( int direction ) {
  switch ( direction ) {
    case constants::RIGHT:
      return Esplora.readButton ( constants::RIGHT ) == LOW || Esplora.readJoystickX () > 255;
    case constants::DOWN:
      return Esplora.readButton ( constants::DOWN ) == LOW || Esplora.readJoystickY () < -255;
    case constants::UP:
      return Esplora.readButton ( constants::UP ) == LOW || Esplora.readJoystickY () > 255;
    case constants::LEFT:
      return Esplora.readButton ( constants::LEFT ) == LOW || Esplora.readJoystickX () < -255;
  }
}

/* Returns true after button pressed and let go
 * From https://courses.fit.cvut.cz/BI-ARD/tutorials/04/index.html
 * Slightly modified to use joystick button for enter
 */
byte buttonFlag = 0;
bool ButtonEvent ( int button ) {
  switch ( button ) {
    case constants::UP:
      if ( Esplora.readButton ( constants::UP ) == LOW )
        buttonFlag |= 1;
      else if ( buttonFlag & 1 ) {
        buttonFlag ^= 1;
        return true;
      }
      break;

    case constants::DOWN:
      if ( Esplora.readButton ( constants::DOWN ) == LOW )
        buttonFlag |= 2;
      else if ( buttonFlag & 2 ) {
        buttonFlag ^= 2;
        return true;
      }
      break;

    case constants::ENTER:
      if ( Esplora.readJoystickButton () == LOW )
        buttonFlag |= 4;
      else if ( buttonFlag & 4 ) {
        buttonFlag ^= 4;
        return true;
      }
  }
  return false;
}

// Don't change direction if no button pressed or the opposite direction pressed
void ChangeDirection ( int & dir ) {
  int newDir = dir;
  if ( ReadDirection ( constants::RIGHT ) )
    newDir = constants::RIGHT;
  else if ( ReadDirection ( constants::LEFT ) )
    newDir = constants::LEFT;
  else if ( ReadDirection ( constants::UP ) )
    newDir = constants::UP;
  else if ( ReadDirection ( constants::DOWN ) )
    newDir = constants::DOWN;

  // Snake can't do a 180 degree turn
  if ( dir == constants::RIGHT && newDir == constants::LEFT )
    return;
  if ( dir == constants::LEFT && newDir == constants::RIGHT )
    return;
  if ( dir == constants::UP && newDir == constants::DOWN )
    return;
  if ( dir == constants::DOWN && newDir == constants::UP )
    return;

  dir = newDir;
}

void RunGame () {
  // Draw 2 pixel wide blue borders
  Display::DrawBorders ( 16, 16, 128 );
  // Clear screen inside borders
  EsploraTFT.noStroke ();
  EsploraTFT.fill ( 0, 0, 0 );
  EsploraTFT.rect ( 2, 2, 158, 124 );

  // Initialize RNG
  Snake::InitRandom ();
  // Orientation of snake
  int direction;
  // Make new snake in a random position oriented towards center
  Snake snake = Snake ( direction, constants::INIT_SNAKE_LEN );

  // Generate first food
  Segment food;
  bool foodValid = false;
  while ( !foodValid ) {
    food.x = ( random ( 0, 38 ) * 4 ) + 2;
    food.y = ( random ( 0, 30 ) * 4 ) + 2;
    // Food cannot be inside snake already
    foodValid = !snake.Occupies ( food );
  }
  
  // Draw first food
  EsploraTFT.fill ( 0, 255, 0 );
  EsploraTFT.rect ( food.x, food.y, 4, 4 );

  SCORE = 0;
  SNAKE_ALIVE = true;
  FOOD_VALUE = 10;
  bool bordersRedrawn = false;
  // Wait a little so that player will have some time to realize the game started
  unsigned long lastMillis = millis () + 300;
  Sound::StartTune ();

  while ( SNAKE_ALIVE ) {
    ChangeDirection ( direction );
    // Wait before moving
    if ( lastMillis + constants::TICK_LENGTH < millis () ) {
      lastMillis = millis ();
      
      if ( !snake.Move ( direction ) ) {
        if ( snake.CollidedWithBorder () && SCORE >= constants::WIN_THRESHOLD ) {
          SCORE += snake.Length () * FOOD_VALUE;
          snake.SlitherAway ();
        }
        else
          SNAKE_ALIVE = false;
        return;
      }

      // Check if snake collided with food
      if ( food.x == snake.Head ().x && food.y == snake.Head ().y ) {
        SCORE += FOOD_VALUE;
        IncreaseFoodValue ();
        snake.Grow ( constants::GROW_UPON_EATING );
        Sound::FoodEaten ();
        // If score sufficient, draw green borders to signalize that snake won't die on contact anymore
        if ( !bordersRedrawn && SCORE >= constants::WIN_THRESHOLD )
        {
          Display::DrawBorders ( 16, 128, 16 );
          bordersRedrawn = true;
        }
        // Generate new food
        foodValid = false;
        /* Keep trying (could be an issue with a really long snake - slow iteration and relying on RNG)
         * Extreme case: 1 valid segment, 1208 occuppied by snake
         */
        while ( !foodValid ) {
          food.x = ( random ( 0, 38 ) * 4 ) + 2;
          food.y = ( random ( 0, 30 ) * 4 ) + 2;
          foodValid = !snake.Occupies ( food );
        }
      }
    }
  }
}

void setup () {
  EsploraTFT.begin ();
  STATE = START;
}

void loop () {
  switch ( STATE ) {
    case START:
      Display::SplashScreen ();
      NEXT_STATE = SPLASH_SCREEN;
      break;

    case SPLASH_SCREEN:
      if ( ButtonEvent ( constants::ENTER ) )
        NEXT_STATE = RUN_GAME;
      break;

    case RUN_GAME:
      // Game ends once snake dies or exits through border
      RunGame ();

      if ( SNAKE_ALIVE )
        Sound::SnakeSurvived ();
      else
        Sound::SnakeDied ();
      Display::EndScreen ( SNAKE_ALIVE, SCORE );
      Display::ReplaySelected ();

      NEXT_STATE = REPLAY_SEL;
      break;

    case REPLAY_SEL:
      if ( ButtonEvent ( constants::DOWN ) )
      {
        Display::ReturnSelected ();
        NEXT_STATE = MAIN_MENU_SEL;
      }
      else if ( ButtonEvent ( constants::ENTER ) )
        NEXT_STATE = RUN_GAME;
      break;

    case MAIN_MENU_SEL:
      if ( ButtonEvent ( constants::UP ) )
      {
        Display::ReplaySelected ();
        NEXT_STATE = REPLAY_SEL;
      }
      else if ( ButtonEvent ( constants::ENTER ) )
        NEXT_STATE = START;
   }
   STATE = NEXT_STATE;
}
