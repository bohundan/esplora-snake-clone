#ifndef SEGMENT_H
#define SEGMENT_H

/* One segment on Esplora's TFT display (160x128) will be 4x4 pixels
 * Valid top left coordinates of each segment are:
 * (x * 4) + 2, x: 0-38
 * (y * 4) + 2, y: 0-30
 * 
 * Examples:
 * 2x2,   6x2,   10x2,   ..  154x2
 * 2x6,   6x6,   10x6,   ..  154x6
 * ...    ...    ...     ..   ...
 * 2x122, 6x122, 10x122, ..  154x122
 */
struct Segment {
  int x; // Top left X coordinate
  int y; // Top left Y coordinate
};

#endif
