#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <Esplora.h>

namespace constants {
  const int RIGHT = SWITCH_RIGHT;
  const int LEFT  = SWITCH_LEFT;
  const int UP    = SWITCH_UP;
  const int DOWN  = SWITCH_DOWN;
  const int ENTER = -16; // Random number, I couldn't find values for SWITCH_DOWN etc. so that it doesn't collide

  const int INIT_SNAKE_LEN   = 3;    // Length of snake at the start of the game (19 max, higher than that and snake won't have space )
  const int GROW_UPON_EATING = 3;    // How many segments snake will grow after eating food
  const int WIN_THRESHOLD    = 2100; // Score equivalent to 20 pieces of food eaten
  const int TICK_LENGTH      = 300;  // How many milliseconds to wait before snake moves again
}

#endif